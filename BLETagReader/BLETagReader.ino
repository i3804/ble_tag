#include <Arduino.h>

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include <BLEEddystoneURL.h>
#include <BLEEddystoneTLM.h>
#include <BLEBeacon.h>
#include "Config.h"
#include "BLECallback.h"
#include "MQTT.h"
#include "WiFiModule.h"

#define ENDIAN_CHANGE_U16(x) ((((x)&0xFF00) >> 8) + (((x)&0xFF) << 8))

int scanTime = 5; //In seconds
BLEScan *BLEScanner;

void setup() {
  Serial.begin(115200);
  delay(10000);
  Serial.println("Scanning...");
  
  connect_to_SSID();
  
  initialize_MQTT();

  BLEDevice::init("");
  BLEScanner = BLEDevice::getScan(); //create new scan
  BLEScanner->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  BLEScanner->setActiveScan(true); //active scan uses more power, but get results faster
  BLEScanner->setInterval(100);
  BLEScanner->setWindow(99); // less or equal setInterval value
}

void loop() {
  BLEScanResults foundDevices = BLEScanner->start(scanTime, false);
  
  Serial.print("Devices found: ");
  Serial.println(foundDevices.getCount());
  Serial.println("Scan done!");
  
  BLEScanner->clearResults(); // delete results fromBLEScan buffer to release memory
}
