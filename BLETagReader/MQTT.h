#pragma once

#include <PubSubClient.h>
#include <WiFi.h>
#include "Config.h"


WiFiClient espClient;
PubSubClient mqtt_cli(espClient);

void initialize_MQTT() {
  mqtt_cli.setServer(mqtt_broker, mqtt_port);
  mqtt_cli.setBufferSize(2048);
  
  while (!mqtt_cli.connected()) {
  String client_id = "esp32-" + String(WiFi.macAddress());
  Serial.print("The client " + client_id);
  Serial.println(" connects to the public mqtt broker\n");
  
  if (mqtt_cli.connect(client_id.c_str())) {
      Serial.println("MQTT Connected");
  } else {
      Serial.print("failed with state ");
      Serial.println(mqtt_cli.state());
      delay(2000);
    }
  }
}
