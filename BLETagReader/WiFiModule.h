#pragma once

#include <Arduino.h>
#include <WiFi.h>
#include "Config.h"

unsigned long WIFI_CONNECTION_TIMEOUT = 20000;

void connect_to_SSID() {
  Serial.println("Establishing connection");
  Serial.printf("Connecting to %s\n", ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  unsigned long startTime = millis();
  int dot_counter = 0;

  while(WiFi.status() != WL_CONNECTED && millis() - startTime < WIFI_CONNECTION_TIMEOUT) {
    Serial.print(".");
    delay(100);
    dot_counter++;

    if (dot_counter % 10 == 0) {
      Serial.print("\n"); 
    }
  }

  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("Connection failed!");  
    return;
  }

  Serial.print("Connected to " + String(ssid) + " with ip ");
  Serial.println(WiFi.localIP());
};
