#pragma once

const char* ssid = "your ssid here"; // SSID of wifi network
const char* password = "password to your ssid here"; // Password to wifi network

const char* mqtt_broker = "broker.hivemq.com";
const int mqtt_port = 1883;
const char* mqtt_topic = "/isu/inventory_tracking/ble/402";
