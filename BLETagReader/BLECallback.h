#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include <BLEEddystoneURL.h>
#include <BLEEddystoneTLM.h>
#include <BLEBeacon.h>
#include "MQTT.h"
#include "Config.h"


#define ENDIAN_CHANGE_U16(x) ((((x)&0xFF00) >> 8) + (((x)&0xFF) << 8))

class MyAdvertisedDeviceCallbacks : public BLEAdvertisedDeviceCallbacks {
private:
  float signal_strength_constant = 4.0; // high strength
  
public:
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      std::string strManufacturerData = advertisedDevice.getManufacturerData();
      uint8_t cManufacturerData[100];
      strManufacturerData.copy((char *)cManufacturerData, strManufacturerData.length(), 0);
      
      BLEBeacon oBeacon = BLEBeacon();
      oBeacon.setData(strManufacturerData);

      String ble_tag = String(ENDIAN_CHANGE_U16(oBeacon.getMajor())) + "-" + String(ENDIAN_CHANGE_U16(oBeacon.getMinor())) + "-" + oBeacon.getProximityUUID().toString().c_str();
      Serial.println("BLE tag is " + ble_tag);
      
      int rssi = advertisedDevice.getRSSI();
      int signal_power = oBeacon.getSignalPower();
      
      float distance = pow(10, (float(signal_power - rssi) / float(10 * this->signal_strength_constant)));

      Serial.println("Rssi is: " + String(rssi));
      Serial.println("Distance to beacon is: " + String(distance));

      String jsonPayload = "{\"tag\": \"" + ble_tag + "\", " + "\"distance\": " + String(distance) + "}";
      Serial.println("Payload json: " + jsonPayload);
      mqtt_cli.publish(mqtt_topic, jsonPayload.c_str());
    }
};
